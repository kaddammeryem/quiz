import React from 'react';
import { StyleSheet, View,Text,Image} from 'react-native';
import {Button,Icon,Card} from 'native-base';
import {createAppContainer} from "react-navigation"
import {createStackNavigator} from "react-navigation-stack";
import Party from "./components/party"






class App extends React.Component  {
  render(){
  return (
    <View style={{flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"orange"}}>
      <View style={{flex:3,justifyContent:"center"}}>
        <Image source={require('./assets/quizzpic.jpeg')}></Image>
      </View>
      <View style={{flex:3}}> 
        <Button onPress={()=>this.props.navigation.navigate("Partie")} rounded primary style={{width:400,justifyContent:"center",marginBottom:15}} ><Text style={{fontSize:30,color:"white"}}>New Game</Text></Button>
        <Button rounded primary style={{width:400,justifyContent:"center",marginBottom:15}} ><Text style={{fontSize:30,color:"white"}}>High Score</Text></Button>
        <Button rounded primary style={{width:400,justifyContent:"center",marginBottom:15}} ><Text style={{fontSize:30,color:"white"}}>About</Text></Button>
        <Button rounded primary style={{width:400,justifyContent:"center"}} ><Text style={{fontSize:30,color:"white"}}>Exit</Text></Button>
      
      </View>
     
      
    
    </View>
  );
}}


  
  

  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const navigator= createStackNavigator({
  Home:App,
  Partie:Party
},
 {
 "initialRouteName":"Home",
 "headerMode":"none"
 }
)

export default createAppContainer(navigator);
